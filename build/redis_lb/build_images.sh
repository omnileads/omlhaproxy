#!/bin/bash

#docker login -u $DOCKER_USER -p $DOCKER_PASSWORD

if [ $CI_COMMIT_REF_NAME == "master" ]; then
	docker build -f Dockerfile -t freetechsolutions/omlhap_redis:latest ./
	docker push freetechsolutions/omlhap_redis:latest
elif [ $CI_COMMIT_REF_NAME == "develop" ]; then
	docker build -f Dockerfile -t freetechsolutions/omlhap_redis:develop ./
	docker push freetechsolutions/omlhap_redis:develop
else 
	docker build -f Dockerfile -t freetechsolutions/omlhap_redis:$1 ./
	docker push freetechsolutions/omlhap_redis:$1
fi
