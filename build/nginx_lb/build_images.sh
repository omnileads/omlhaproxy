#!/bin/bash

#docker login -u $DOCKER_USER -p $DOCKER_PASSWORD

if [ $CI_COMMIT_REF_NAME == "master" ]; then
	docker build -f Dockerfile -t freetechsolutions/omlhap_app:latest ./
	docker push freetechsolutions/omlhap_app:latest
elif [ $CI_COMMIT_REF_NAME == "develop" ]; then
	docker build -f Dockerfile -t freetechsolutions/omlhap_app:develop ./
	docker push freetechsolutions/omlhap_app:develop
else
	docker build -f Dockerfile -t freetechsolutions/omlhap_app:$1 ./
	docker push freetechsolutions/omlhap_app:$1		
fi
