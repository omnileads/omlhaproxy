#!/bin/bash

 # --- OMniLeads Web App nodes to add to cluster
# export oml_app_01_host=
# export oml_app_02_host=

# --- OMniLeads REDIS nodes to add to cluster
# export oml_redis_01_host=
# export oml_redis_02_host=
# export oml_redis_03_host=
# export oml_redis_04_host=

# export oml_haproxy_branch=main

# # Set your net interfaces, you must have at least a PRIVATE_NIC (eth0, enp0s3 ...)
# export oml_nic=

# export oml_deploy_ha=true
# node role values: main | backup
# export oml_ha_rol=
# --- The Virtual IP address
# export oml_ha_vip=
# --- The Network interface NIC for Virtual Public or Private IP addr (eth0, enp0s3 ...)
# export oml_ha_vip_nic=
# --- Tenant name
# export oml_ha_tenant=
# --- Email to send notification in case of fails
# export oml_ha_email=

SRC=/usr/src
COMPONENT_REPO=https://gitlab.com/omnileads/omlhaproxy.git
PATH_CERTS="$(cd "$(dirname "$BASH_SOURCE")" &> /dev/null && pwd)/certs"

echo "************************ disable SElinux *************************"
echo "************************ disable SElinux *************************"
sed -i 's/^SELINUX=.*/SELINUX=disabled/' /etc/sysconfig/selinux
sed -i 's/^SELINUX=.*/SELINUX=disabled/' /etc/selinux/config
setenforce 0
systemctl disable firewalld > /dev/null 2>&1
systemctl stop firewalld > /dev/null 2>&1
echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf

yum -y install epel-release git python3 python3-pip libselinux-python3 make autoconf automake gcc

echo "************************ install ansible *************************"
echo "************************ install ansible *************************"
echo "************************ install ansible *************************"
pip3 install pip --upgrade
pip3 install 'ansible==2.9.2'
export PATH="$HOME/.local/bin/:$PATH"

# cat oml.crt oml.key > hap.pem

echo "************************ clone REPO *************************"
echo "************************ clone REPO *************************"
cd $SRC
git clone $COMPONENT_REPO
cd omlhaproxy
git checkout ${oml_haproxy_branch}
cd deploy

sed -i "s/omlapp_01_hostname=/omlapp_01_hostname=${oml_app_01_host}/g" ./inventory
sed -i "s/omlapp_02_hostname=/omlapp_02_hostname=${oml_app_02_host}/g" ./inventory
sed -i "s/omlredis_01_hostname=/omlredis_01_hostname=${oml_redis_01_host}/g" ./inventory
sed -i "s/omlredis_02_hostname=/omlredis_02_hostname=${oml_redis_02_host}/g" ./inventory
sed -i "s/omlredis_03_hostname=/omlredis_03_hostname=${oml_redis_03_host}/g" ./inventory
sed -i "s/omlredis_04_hostname=/omlredis_04_hostname=${oml_redis_04_host}/g" ./inventory

if [[ "${oml_deploy_ha}" == "true" ]];then
sed -i "s/#deploy_ha=/deploy_ha=true/g" ./inventory
sed -i "s/#ha_rol=/ha_rol=${oml_ha_rol}/g" ./inventory
sed -i "s%\#ha_vip=%ha_vip=${oml_ha_vip}%g" ./inventory
sed -i "s/#ha_vip_nic=/ha_vip_nic=${oml_ha_vip_nic}/g" ./inventory
sed -i "s/#ha_notification_email=/ha_notification_email=${oml_ha_email}/g" ./inventory
sed -i "s/#ha_tenant=/ha_tenant=${oml_ha_tenant}/g" ./inventory

if [ -f $PATH_CERTS/hap.pem ];then
cp $PATH_CERTS/hap.pem $SRC/omlhaproxy/certs/hap_custom.pem
sed -i "s/#ssl_custom=true/ssl_custom=true/g" ./inventory
fi

echo "net.ipv4.ip_nonlocal_bind = 1"  >> /etc/sysctl.conf
sysctl -p
fi

ansible-playbook haproxy.yml -i inventory
