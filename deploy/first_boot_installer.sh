    #!/bin/bash

    export oml_app_01_host=172.16.20.71
    export oml_app_02_host=172.16.20.72

    export oml_redis_01_host=172.16.20.61
    export oml_redis_02_host=172.16.20.62
    export oml_redis_03_host=172.16.20.63
    export oml_redis_04_host=172.16.20.64

    export oml_haproxy_branch=main

    # Set your net interfaces, you must have at least a PRIVATE_NIC
    export oml_nic=eth0

    export oml_deploy_ha=true
    export oml_ha_rol=main
    export oml_ha_vip=172.16.20.84
    export oml_ha_vip_nic=eth0
    export oml_ha_tenant=bardas
    export oml_ha_email=fabian.pignataro@freetechsolutions.com.ar

    SRC=/usr/src
    COMPONENT_REPO=https://gitlab.com/omnileads/omlhaproxy.git
    PATH_CERTS="$(cd "$(dirname "$BASH_SOURCE")" &> /dev/null && pwd)/certs"

    echo "************************ disable SElinux *************************"
    echo "************************ disable SElinux *************************"
    sed -i 's/^SELINUX=.*/SELINUX=disabled/' /etc/sysconfig/selinux
    sed -i 's/^SELINUX=.*/SELINUX=disabled/' /etc/selinux/config
    setenforce 0
    systemctl disable firewalld > /dev/null 2>&1
    systemctl stop firewalld > /dev/null 2>&1
    echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf

    yum -y install epel-release git python3 python3-pip libselinux-python3 make autoconf automake gcc

    echo "************************ install ansible *************************"
    echo "************************ install ansible *************************"
    echo "************************ install ansible *************************"
    pip3 install pip --upgrade
    pip3 install 'ansible==2.9.2'
    export PATH="$HOME/.local/bin/:$PATH"

    # cat oml.crt oml.key > hap.pem

    echo "************************ clone REPO *************************"
    echo "************************ clone REPO *************************"
    cd $SRC
    git clone $COMPONENT_REPO
    cd omlhaproxy
    git checkout ${oml_haproxy_branch}
    cd deploy

    sed -i "s/omlapp_01_hostname=/omlapp_01_hostname=${oml_app_01_host}/g" ./inventory
    sed -i "s/omlapp_02_hostname=/omlapp_02_hostname=${oml_app_02_host}/g" ./inventory
    sed -i "s/omlredis_01_hostname=/omlredis_01_hostname=${oml_redis_01_host}/g" ./inventory
    sed -i "s/omlredis_02_hostname=/omlredis_02_hostname=${oml_redis_02_host}/g" ./inventory
    sed -i "s/omlredis_03_hostname=/omlredis_03_hostname=${oml_redis_03_host}/g" ./inventory
    sed -i "s/omlredis_04_hostname=/omlredis_04_hostname=${oml_redis_04_host}/g" ./inventory

    if [[ "${oml_deploy_ha}" == "true" ]];then
    sed -i "s/#deploy_ha=/deploy_ha=true/g" ./inventory
    sed -i "s/#ha_rol=/ha_rol=${oml_ha_rol}/g" ./inventory
    sed -i "s%\#ha_vip=%ha_vip=${oml_ha_vip}%g" ./inventory
    sed -i "s/#ha_vip_nic=/ha_vip_nic=${oml_ha_vip_nic}/g" ./inventory
    sed -i "s/#ha_notification_email=/ha_notification_email=${oml_ha_email}/g" ./inventory
    sed -i "s/#ha_tenant=/ha_tenant=${oml_ha_tenant}/g" ./inventory

    if [ -f $PATH_CERTS/hap.pem ];then
    cp $PATH_CERTS/hap.pem $SRC/omlhaproxy/deploy/certs/hap_custom.pem
    fi

    echo "net.ipv4.ip_nonlocal_bind = 1"  >> /etc/sysctl.conf
    sysctl -p
    fi

    ansible-playbook haproxy.yml -i inventory
